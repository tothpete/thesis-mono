const state = {
  editableId: ''
}

const getters = {
}

const actions = {
  setEditable({commit}, { wbcId }) {
    commit('setEditableId', wbcId)
  },
  unsetEditable({commit}) {
    commit('setEditableId', null)
  }
}

const mutations = {
  setEditableId(state, payload) {
    state.editableId = payload
  }
}

export default { namespaced: true, state, getters, actions, mutations }