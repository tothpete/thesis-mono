const state = {
  selectedId: ''
}

const getters = {
  isSelected: (state) => ({ wbcId }) => wbcId == state.selectedId
}

const actions = {
  setSelected({ commit }, { wbcId }) {
    commit('setSelectedId', wbcId)
  }
}

const mutations = {
  setSelectedId(state, payload) {
    state.selectedId = payload
  }
}

export default { namespaced: true, state, getters, actions, mutations }