const state = {
  profile: {}
}

const getters = {
  loggedIn(state) {
    return !!state.profile.sub
  },
  avatar(state) {
    return state.profile.picture
  }
}

const actions = {
}

const mutations = {
  setUser(state, user) {
    state.user = user
  },
  setProfile(state, profile) {
    state.profile = profile
  }
}

export default { namespaced: true, state, actions, mutations, getters }