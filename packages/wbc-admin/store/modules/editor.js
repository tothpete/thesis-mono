import selectable from './editor/selectable'
import editable from './editor/editable'

const state = {
  isEditingText: false,
  isSettingsOpen: false,
  isResizing: false
}

const getters = {
}

const actions = {
}

const mutations = {
  setIsEditingText(state, payload) {
    state.isEditingText = payload
  },
  setIsSettingsOpen(state, payload) {
    state.isSettingsOpen = payload
  },
  setIsResizing(state, payload) {
    state.isResizing = payload
  }
}

const modules = {
  selectable,
  editable
}

export default { namespaced: true, state, getters, actions, mutations, modules }