import Vuex from 'vuex'

import websites from 'wbc-models/store/websites'
import componentTree from 'wbc-components/store/component-tree'
import mountedComponents from 'wbc-components/store/mounted-components'
import editor from './modules/editor'

const createStore = () => {
  return new Vuex.Store({
    modules: {
      editor,
      websites,
      componentTree,
      mountedComponents
    }
  })
}

export default createStore
