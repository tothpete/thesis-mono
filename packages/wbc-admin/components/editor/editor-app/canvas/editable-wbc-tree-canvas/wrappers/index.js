import wbcButtonWrapper from './wbc-button-wrapper'
import wbcContainerWrapper from './wbc-container-wrapper'
import wbcFooterWrapper from './wbc-footer-wrapper'
import wbcHeaderWrapper from './wbc-header-wrapper'
import wbcImageWrapper from './wbc-image-wrapper'
import wbcNavWrapper from './wbc-nav-wrapper'
import wbcPageWrapper from './wbc-page-wrapper'
import wbcRootWrapper from './wbc-root-wrapper'
import wbcSectionWrapper from './wbc-section-wrapper'
import wbcTextWrapper from './wbc-text-wrapper'
import modules from './modules'

export default {
  'wbc-button': wbcButtonWrapper,
  'wbc-container': wbcContainerWrapper,
  'wbc-footer': wbcFooterWrapper,
  'wbc-header': wbcHeaderWrapper,
  'wbc-image': wbcImageWrapper,
  'wbc-nav': wbcNavWrapper,
  'wbc-page': wbcPageWrapper,
  'wbc-root': wbcRootWrapper,
  'wbc-section': wbcSectionWrapper,
  'wbc-text': wbcTextWrapper,
  modules
}