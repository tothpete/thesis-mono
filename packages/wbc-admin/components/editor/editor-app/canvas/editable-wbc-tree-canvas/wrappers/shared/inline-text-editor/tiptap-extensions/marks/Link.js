import { Mark, Plugin } from 'tiptap'
import { updateMark, removeMark, pasteRule } from 'tiptap-commands'
import { getMarkRange } from 'tiptap-utils'

export default class Link extends Mark {
  get name() {
    return 'link'
  }

  get schema() {
    return {
      attrs: {
        href: {
          default: null,
        },
      },
      inclusive: false,
      parseDOM: [
        {
          tag: 'a[href]',
          getAttrs: dom => ({
            href: dom.getAttribute('href') || '#',
            linkTo: dom.getAttribute('data-linkto'),
            payload: dom.getAttribute('data-payload')
          }),
        },
      ],
      toDOM: node => ['a', {
        rel: 'noopener noreferrer nofollow',
        class: 'wbc-a',
        href: '#',
        'data-linkto': node.attrs.linkTo,
        'data-payload': node.attrs.payload,
      }, 0],
    }
  }

  commands({ type }) {
    // style
      // color
      // underline
      // backdrop
      // on hover effect
        // color
        // underline
        // backdrop
        // 
    return attrs => {
      if (attrs.linkTo) {
        return updateMark(type, attrs)
      }

      return removeMark(type)
    }
  }

  get plugins() {
    return [
      // new Plugin({
      //   props: {
      //     handleClick(view, pos) {
      //       const { schema, doc, tr } = view.state
      //       const range = getMarkRange(doc.resolve(pos), schema.marks.link)

      //       if (!range) {
      //         return
      //       }

      //       const $start = doc.resolve(range.from)
      //       const $end = doc.resolve(range.to)
      //       const transaction = tr.setSelection(new TextSelection($start, $end))

      //       view.dispatch(transaction)
      //     },
      //   },
      // }),
    ]
  }

}