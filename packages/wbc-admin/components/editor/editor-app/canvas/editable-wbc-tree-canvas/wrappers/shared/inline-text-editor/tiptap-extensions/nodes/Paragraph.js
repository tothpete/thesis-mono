import { setBlockType, toggleBlockType } from 'tiptap-commands'
import { Node } from 'tiptap'
import textCommands from './commands/text-commands'

export default class Paragraph extends Node {
  constructor() {
    super()
    this.commands = textCommands
  }

  get name() {
    return 'paragraph'
  }

  get schema() {
    return {
      attrs: {
        textAlign: {
          default: 'left'
        }
      },
      content: 'inline*',
      group: 'block',
      draggable: false,
      parseDOM: [{
        tag: 'p',
        getAttrs: node => {
          return {
            textAlign: node.style.textAlign || 'left'
          }
        }
      }],
      toDOM: node => ['p', { 
        class: 'wbc-p',
        style: `text-align:${node.attrs.textAlign}`
      }, 0],
    }
  }
}