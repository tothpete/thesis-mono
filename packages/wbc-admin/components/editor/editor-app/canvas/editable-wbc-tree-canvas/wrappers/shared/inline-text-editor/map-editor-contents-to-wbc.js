import WbcP from 'wbc-components/components/wbc_p'
import WbcHeading from 'wbc-components/components/wbc_heading'
import WbcTn from 'wbc-components/components/wbc_tn'
import WbcText from 'wbc-components/components/wbc_text'

const mapToWbcText = ({}) => {
  return new WbcText()
}

const mapToWbcHeading = ({ attrs={} }) => {
  return new WbcHeading({
    styles: { textAlign: attrs.textAlign },
    settings: { level: attrs.level }
  })
}

const mapToWbcP = ({ attrs={} }) => {
  return new WbcP({
    styles: { textAlign: attrs.textAlign }
  })
}

const mapToWbcTn = ({ text, attrs={}, marks=[] }) => {
  return new WbcTn({ settings: { content: text } })
}

const getWbcComponent = (element) => {
  const { type } = element
  if (type == 'doc') return mapToWbcText(element)
  if (type == 'heading') return mapToWbcHeading(element)
  if (type == 'paragraph') return mapToWbcP(element)
  if (type == 'text') return mapToWbcTn(element)

  throw new Error(`Mapping for element type=${type} not found`)
}

export default function mapEditorContentsToWbc(currentElement) {
  let wbcComponent = getWbcComponent(currentElement)
  wbcComponent.children = (currentElement.content || []).map(el => mapEditorContentsToWbc(el))
  return wbcComponent
}