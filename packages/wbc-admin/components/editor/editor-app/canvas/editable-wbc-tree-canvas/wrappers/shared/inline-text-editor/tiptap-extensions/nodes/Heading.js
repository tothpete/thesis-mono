import { Node } from 'tiptap'
import { setBlockType, textblockTypeInputRule, toggleBlockType } from 'tiptap-commands'
import textCommands from './commands/text-commands'

export default class Heading extends Node {
  constructor() {
    super()
    this.commands = textCommands
  }

  get name() {
    return 'heading'
  }

  get defaultOptions() {
    return {
      levels: [1, 2, 3],
    }
  }

  get schema() {
    return {
      attrs: {
        level: {
          default: 1,
        },
        textAlign: {
          default: 'left'
        }
      },
      content: 'inline*',
      group: 'block',
      defining: true,
      draggable: false,
      parseDOM: this.options.levels
        .map(level => ({
          tag: `h${level}`,
          getAttrs: node => {
            return {
              level,
              textAlign: node.style.textAlign || 'left'
            }
          }
        })),
      toDOM: node => [`h${node.attrs.level}`, { 
        class: 'wbc-heading',
        style: `text-align:${node.attrs.textAlign}`
      }, 0],
    }
  }
}