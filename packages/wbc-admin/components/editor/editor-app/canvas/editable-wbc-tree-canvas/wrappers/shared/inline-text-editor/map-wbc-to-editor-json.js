const mappings = {
  'wbc-text': (component, children) => {
    return {
      type: 'doc',
      attrs: {
        // line height etc
        // maybe language
      },
      content: children
    }
  },
  'wbc-sitetitle': (component, children) => {
    return {
      type: 'doc',
      attrs: {
        // line height etc
        // maybe language
      },
      content: children
    }
  },
  'wbc-heading': (component, children) => {
    return {
      type: 'heading',
      attrs: {
        level: component.settings.level,
        textAlign: component.styles.textAlign
      },
      content: children
    }
  },
  'wbc-p': (component, children) => {
    return {
      type: 'paragraph',
      attrs: {
        textAlign: component.styles.textAlign
      },
      content: children
    }
  },
  'wbc-tn': (component) => {
    return {
      type: 'text',
      text: component.settings.content
    }
  },
}

export default function mapWbcToEditorJson(currentElement) {
  const children = currentElement.children.length > 0 ? currentElement.children.map(child => mapWbcToEditorJson(child)) : [{ type: 'paragraph' }]
  return mappings[currentElement.name](currentElement, children)
}
