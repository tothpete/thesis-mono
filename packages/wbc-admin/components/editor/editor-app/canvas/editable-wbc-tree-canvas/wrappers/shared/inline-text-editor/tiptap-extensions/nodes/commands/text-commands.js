import { toggleBlockType } from 'tiptap-commands'

export default function({ type, schema }) {
  return attrs => {
    if (attrs.textAlign) {
      return (state, dispatch) => {
        const { from, to, $cursor } = state.selection

        let nodes = []

        state.doc.nodesBetween(from, to, (node, pos) => {
          if (node.type.name == 'paragraph' || node.type.name == 'heading') nodes.push({ node, pos })
        })
      
        const { tr } = state

        nodes.forEach(node => {
          tr.setBlockType(
            node.pos, 
            ++node.pos, 
            state.schema.nodes[node.node.type.name],
            Object.assign({}, node.node.attrs, { textAlign: attrs.textAlign })
          )
        })

        dispatch(tr)
      }
    } else {
      return toggleBlockType(type, schema.nodes.paragraph, attrs)
    }
  }
}