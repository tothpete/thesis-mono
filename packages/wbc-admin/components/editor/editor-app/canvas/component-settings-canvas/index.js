import buttonSettings from './button-settings'
import headerSettings from './header-settings'
import imageSettings from './image-settings'
import navSettings from './nav-settings'
import sectionSettings from './section-settings'

export default {
  'wbc-button': buttonSettings,
  'wbc-header': headerSettings,
  'wbc-image': imageSettings,
  'wbc-nav': navSettings,
  'wbc-section': sectionSettings
}