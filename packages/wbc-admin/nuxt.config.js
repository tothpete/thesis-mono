const nodeExternals = require('webpack-node-externals')

module.exports = {
  mode: 'spa',
  dev: (process.env.NODE_ENV !== 'production'),
  modules: [
    '@nuxtjs/axios',
  ],
  plugins: [
    '~/plugins/element-ui',
    '~/plugins/color-picker',
    '~/plugins/dropzone',
    '~/plugins/font-awesome',
    '~/plugins/mouse-proximities',
    '~/plugins/ui-components',
    '~/plugins/vue-js-modal',
    '~/plugins/v-tooltip',
  ],
  serverMiddleware: [
    '~/middleware/selective-ssr'
  ],
  router: {
    middleware: [
    ]
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Website builder',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Website builder' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  /*
  ** Customize the progress-bar color
  */
  loading: false,
  /*
   ** Build configuration
   */
  build: {
    vendor: ['axios'],
    /*
     ** Run ESLINT on save
     */
    extend (config, ctx) {
      const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')

      config.plugins.push(new FriendlyErrorsWebpackPlugin())

      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^wbc-components/, /^wbc-themes/, /^wbc-models/, /^wbc-ui/]
          })
        ]
      }

      config.resolve.alias['vue'] = 'vue/dist/vue.common'
    }
  },
}
