import Vue from 'vue'

import components from 'wbc-ui/views'

Object.keys(components).forEach(component => {
  Vue.component(component, components[component])
})