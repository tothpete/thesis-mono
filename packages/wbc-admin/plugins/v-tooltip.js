import Vue from 'vue'
import VTooltip from 'v-tooltip'
import '~/assets/styles/vendor/v-tooltip.scss'

Vue.use(VTooltip, { defaultClass: 'ui-tooltip' })