import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faAlignLeft,
  faAlignCenter,
  faAlignRight,
  faArrowLeft,
  faArrowDown,
  faArrowUp,
  faBold,
  faCog,
  faCircle,
  faItalic,
  faLink,
  faList,
  faListOl,
  faPen,
  faPlusCircle,
  faRedo,
  faTextHeight,
  faTrash,
  faUnderline,
  faUndo,
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faAlignLeft,
  faAlignCenter,
  faAlignRight,
  faArrowLeft,
  faArrowDown,
  faArrowUp,
  faBold,
  faCog,
  faCircle,
  faItalic,
  faLink,
  faList,
  faListOl,
  faPen,
  faPlusCircle,
  faRedo,
  faTextHeight,
  faTrash,
  faUnderline,
  faUndo
)

Vue.component('font-awesome-icon', FontAwesomeIcon)