import Vue from 'vue'
import throttle from 'lodash/throttle'

class MouseProximities {
  constructor({ document }) {
    this.document = document
    Vue.set(this, 'proximities', [])
  }

  initEditorHoverListener() {
    document
      .querySelector('.editor-app')
      .addEventListener('mousemove', throttle(e => this.evaluateDistances(e), 60))
  }

  destroyEditorHoverListener() {
    document
    .querySelector('.editor-app')
    .removeEventListener('mousemove', throttle(e => this.evaluateDistances(e), 60))  
  }

  evaluateDistances(e) {
    this.proximities.forEach(proximity => this.recaulcuateDistance(proximity, e.pageX, e.pageY))
  }

  add(proximity) {
    this.proximities.splice(this.proximities.length, 0, proximity)
  }

  recaulcuateDistance(proximity, mouseX, mouseY) {
    const { el } = proximity
    const rect = el.getBoundingClientRect()
    const offsetTop = rect.top + document.documentElement.scrollTop
    const offsetLeft = rect.left + document.documentElement.scrollLeft

    const x = mouseX - (offsetLeft+(el.offsetWidth/2))
    const y = mouseY - (offsetTop+(el.offsetHeight/2))
    const distance = Math.floor(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)))

    Vue.set(proximity, 'x', x)
    Vue.set(proximity, 'y', y)
    Vue.set(proximity, 'distance', distance)
  }

  remove({ id }) {
    const proximity = this.proximities.find(p => p.id == id)
    this.proximities.splice(this.proximities.indexOf(proximity), 1)
  }
}

export default ({}, inject) => {
  inject('wbcMouseProximities', new MouseProximities({ document }))
}