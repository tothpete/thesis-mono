import mongoose from 'mongoose'

import { db } from '../config/mongo'

export default () => {
  mongoose.connect(db, { useNewUrlParser: true })
}