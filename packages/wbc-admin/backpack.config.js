const nodeExternals = require('webpack-node-externals')

module.exports = {
  webpack: (config, options, webpack) => {
    // config.stats = 'verbose'

    config.entry.main = ['./server/index.js']
      
    config.externals = [
      nodeExternals({
        whitelist: [/^wbc-components/, /^wbc-themes/, /^wbc-models/, /^wbc-ui/]
      })
    ]

    // config.module.rules.push(
    //   {
    //     test: /\.js$/,
    //     exclude: /node_modules\/(?!(wbc-themes|wbc-models)\/).*/,
    //     use: {
    //       loader: 'babel-loader'
    //     }
    //   }
    // )

    return config
  }
}
