import api from './api'

export default function(app) {
  app.use(api)
}