import { Router } from 'express'
import { Website, Page, Layout } from 'wbc-models/models'
import { layout, page } from 'wbc-components/factories/wbc-factory'

const router = Router()

const asyncMiddleware = fn => (req, res, next) => { Promise.resolve(fn(req, res, next)).catch(next) }

router.post('/', asyncMiddleware(async (req, res) => {
  const website = await Website.create({
    name: req.body.name,
    pages: [new Page({ slug: 'home', root: page() })],
    layout: new Layout({ root: layout() })
  })

  res.send({ website })
}))


router.get('/', asyncMiddleware(async (req, res) => {
  const websites = await Website
    .find({})
    .populate('resources.images')

  res.send({ websites })
}))

router.get('/:id', asyncMiddleware(async (req, res) => {
  const website = await Website
    .findOne({ _id: req.params.id })

  if (website) {
    res.send({ websites: [website] })
  } else {
    res.sendStatus(404)
  }
}))

router.patch('/:id', asyncMiddleware(async (req, res) => {
  const website = await Website
    .findOneAndUpdate({ _id: req.params.id }, req.body)
    
  res.sendStatus(200)
}))

export default router