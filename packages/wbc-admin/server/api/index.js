import { Router } from 'express'
import websites from './websites'

const router = Router()

router.use('/websites', websites)

export default router
