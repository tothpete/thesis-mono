import { Website, Layout, Page, Fontkit, Resources } from '../models'

export const createWebsite = async ({ name, userProfile, themeName, layouts=[], pages=[], fontkit, meta={}, defaultComponents={}, resources={} }) => {
  const website = new Website({
    name,
    userProfile,
    themeName,
    layouts: layouts.map(layout => new Layout(layout)),
    pages: pages.map(page => new Page(page)),
    fontkit: new Fontkit(fontkit),
    resources: {
      images: (resources.images || []).map(image => new Resources.Image(image)),
    }
  })

  await website.save()
  await website.fontkit.save()
  await Promise.all(website.layouts.map(layout => layout.save()))
  await Promise.all(website.pages.map(page => page.save()))
  await Promise.all(website.resources.images.map(image => image.save()))

  return website
}