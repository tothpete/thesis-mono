import mongoose from 'mongoose'
import wbcComponents from 'wbc-components/components'

const wbcComponentSchema = mongoose.Schema({
  name: { 
    type: String, 
    required: true, 
    enum: Object.keys(wbcComponents)
  },
  wbcId: { type: String, required: true },
  settings: { type: Object },
  modules: {
    type: Object
  },
  resources: {
    type: Object
  },
  styles: { type: Object }
})

wbcComponentSchema.add({
  children: [wbcComponentSchema],
})

wbcComponentSchema.pre('validate', function(next) {
  next()
})

export default wbcComponentSchema