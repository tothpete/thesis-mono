import mongoose from 'mongoose'
import WbcComponentSchema from './wbc_component.schema'
import WbcRoot from 'wbc-components/components/wbc_root'

const schema = mongoose.Schema({
  name: { type: String, required: true, default: 'default' },
  settings: { type: Object },
  root: { type: WbcComponentSchema, default: new WbcRoot(), required: true },
  draft: { type: WbcComponentSchema },
})

// schema.index({ website: 1, name: 1 }, { unique: true })

export default schema