import mongoose from 'mongoose'
import WbcComponentSchema from './wbc_component.schema'
import WbcPage from 'wbc-components/components/wbc_page'

export default mongoose.Schema({
  slug:       { type: String, required: true, minlength: 1, maxlength: 64 },
  layout:     { type: String },
  root:       { type: WbcComponentSchema, default: new WbcPage(), required: true },
  draft:      { type: WbcComponentSchema },
  home:       { type: String, default: false },
})