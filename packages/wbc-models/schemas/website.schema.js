import mongoose from 'mongoose'

import LayoutSchema from '../schemas/layout.schema'
import PageSchema from '../schemas/page.schema'

const schema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  layout: LayoutSchema,
  pages: [PageSchema]
})

schema.pre('save', function() {
  next()
})

export default schema