import { schema } from 'normalizr'

const layout = new schema.Entity('layouts', {}, { idAttribute: '_id' })
const page = new schema.Entity('pages', {}, { idAttribute: '_id' })

const website = new schema.Entity('websites', {
  layout: layout,
  pages: [page],
}, { idAttribute: '_id' })

export default website