import Vue from 'vue'
import { normalize, denormalize } from 'normalizr'
import websiteEntity from './schema/website'

const state = {
  entities: {
    websites: {},
    layouts: {},
    pages: {},
    fontkits: {},
    resources: {}
  }
}

const getters = {
}

const normalizeWebsites = data => {
  const schema = { websites: [websiteEntity] }
  const { entities } = normalize(data, schema)
  return entities
}

const actions = {
  async fetchWebsites({ commit }) {
    const { data } = await this.$axios.get(`/api/websites`)
    const entities = normalizeWebsites(data)
    commit('updateEntities', { entities })
  },
  async fetchWebsite({ commit }, id) {
    const { data } = await this.$axios.get(`/api/websites/${id}`)
    const entities = normalizeWebsites(data)
    commit('updateEntities', { entities })    
  },
  async saveWebsite({ state }, id) {
    const input = { websites: [id] }
    const schema = { websites: [websiteEntity] }
    const entities = state.entities
    const { websites } = denormalize(input, schema, entities)
    const website = websites[0]

    await this.$axios.patch(`/api/websites/${id}`, website)
  }
}

const mutations = {
  updateEntities (state, {entities}) {
    for (let type in entities) {
      for (let entity in entities[type]) {
        const oldObj = state.entities[type][entity] || {}
        const newObj = Object.assign(oldObj, entities[type][entity])
        Vue.set(state.entities[type], entity, newObj)
      }
    }
  }
}

const modules = {
}

export default { namespaced: true, state, getters, actions, mutations, modules }