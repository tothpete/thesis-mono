import cloudinary from 'cloudinary-core'

const client = cloudinary.Cloudinary.new({ cloud_name: 'websetter' })

export default client