import Page from 'wbc-models/models/page'
import Fontkit from 'wbc-models/models/fontkit'

export const mapFontkit = (object) => {
  return new Fontkit(object)
}

export const mapPage = (object) => {
  return new Page(object)
}