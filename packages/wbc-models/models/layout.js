import mongoose from 'mongoose'
import LayoutSchema from '../schemas/layout.schema'

const Layout = mongoose.model('Layout', LayoutSchema)

export default Layout