import mongoose from 'mongoose'
import WebsiteSchema from '../schemas/website.schema'

WebsiteSchema.pre('remove', function(next) {
  Fontkit.remove({ website: this }).exec()
  Page.remove({ website: this }).exec()
  // resources remove
  next()
})

const Website = mongoose.model('Website', WebsiteSchema)

export default Website