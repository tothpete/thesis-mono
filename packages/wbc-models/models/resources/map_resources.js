import { resources } from 'wbc-models/models'

export default (resources) => {
  return {
    images: (resources.images || []).map(res => new resources.Image(res)),
    navigations: (resources.navigations || []).map(res => new resources.Navigation(res))
  }
}