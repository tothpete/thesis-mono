import WbcComponent from './wbc_component'

export default class WbcHeading extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-heading'
    super(options)
  }

  defaultSettings() {
    return {
      level: 2
    }
  }

  viewProps() {
    return {
      level: this.settings.level
    }
  }

  // classStyles() {
  //   const sizes = ['48px', '36px', '28px', '18px']

  //   return {
  //     fontSize: sizes[this.settings.level]
  //   }
  // }
}
