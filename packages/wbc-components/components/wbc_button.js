import WbcComponent from './wbc_component'

export default class WbcButton extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-button'
    super(options)
  }

  defaultSettings() {
    return {
      content: 'Button text'
    }
  }

  viewProps() {
    return {
      content: this.settings.content,
      url: this.settings.url
    }
  }
}
