import WbcComponent from './wbc_component'

export default class WbcFooter extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-footer'
    super(options)
  }

  scope() {
    return 'footer'
  }

  classStyles() {
    return {
      paddingTop: '2rem',
      paddingBottom: '2rem'
    }
  }

  container() {
    return this.children.filter(child => child.scope() == 'container')[0]
  }
}
