import WbcComponent from './wbc_component'

export default class WbcRoot extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-root'
    super(options)
  }
}
