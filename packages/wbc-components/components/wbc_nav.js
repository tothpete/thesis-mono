import WbcComponent from './wbc_component'

export default class WbcNav extends WbcComponent {
  constructor(options = {}) {
    options.name = 'wbc-nav'
    super(options)
  }

  defaultSettings() {
    return {
      itemSpacing: 20
    }
  }

  viewProps() {
    return {
      // items: this.navigation().items
    }
  }

  scopeProps() {
    return {
      default: {
        style: {
          marginRight: `${this.settings.itemSpacing}px`
        }
      }
    }
  }
}
