import WbcComponent from './wbc_component'

export default class WbcImage extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-image'
    super(options)
  }

  viewProps() {
    return {
      url: this.settings.url
    }
  }
}
