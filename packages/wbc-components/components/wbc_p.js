import WbcComponent from './wbc_component'

export default class WbcP extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-p'
    super(options)
  }
}
