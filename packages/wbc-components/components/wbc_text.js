import WbcComponent from './wbc_component'

export default class WbcText extends WbcComponent {
  constructor(options = {}) {
    options.name = 'wbc-text'
    super(options)
  }
}
