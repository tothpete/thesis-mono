import WbcComponent from './wbc_component'

export default class WbcNavItem extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-nav-item'
    super(options)
  }

  defaultSettings() {
    return {
      type: 'text',
      content: 'Button text',
      url: '#'
    }
  }

  viewProps() {
    return {
      type: this.settings.type,
      content: this.settings.content,
      url: this.settings.url
    }
  }

  scope() {
    return 'default'
  }
}
