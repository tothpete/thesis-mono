import WbcComponent from './wbc_component'

export default class WbcTn extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-tn'
    super(options)
  }

  viewProps() {
    return {
      content: this.settings.content
    }
  }
}
