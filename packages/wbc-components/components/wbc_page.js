import WbcComponent from './wbc_component'

export default class WbcPage extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-page'
    super(options)
  }

  scope() {
    return 'page'
  }
}
