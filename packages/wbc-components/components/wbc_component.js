import uuid from 'uuid/v1'

export default class WbcComponent {
  constructor(component={}) {
    if (!component.name) throw new Error(`WbcComponent name must be specified: ${component}`)

    this.name = component.name
    this.wbcId = component.wbcId || uuid()
    this.settings = Object.assign(this.defaultSettings(), component.settings)
    this.modules = Object.assign(this.classModules(), component.modules)
    this.resources = component.resources || {}
    this.styles = component.styles || {}
    this.children = component.children || []
  }
  
  scope() {
    return
  }

  defaultSettings() {
    return {}
  }

  defaultResources() {
    return {}
  }

  scopeProps() {
    return {}
  }

  viewName() {
    return this.view || this.name
  }

  data() {
    return {
      class: this.classes(),
      key: this.wbcId,
      props: this.props(),
      style: this.allStyles()
    }
  }

  props() {
    let props = {}
    return Object.assign(props, this.viewProps(), { scopeProps: this.scopeProps() })
  }

  viewProps() {
    return {}
  }

  classes() {
    let classes = {}
    classes[this.name] = true
    return classes
  }

  allStyles() {
    return { ...this.classStyles(), ...this.styles }
  }

  classStyles() {
    return {}
  }

  classModules() {
    return {}
  }
}
