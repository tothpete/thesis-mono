import WbcComponent from './wbc_component'
import Backgroundable from './modules/backgroundable'
import Paddingable from './modules/paddingable'

export default class WbcSection extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-section'
    super(options)
  }

  defaultSettings() {
    return {
      mode: 'normal'
    }
  }

  classModules() {
    return {
      backgroundable: new Backgroundable({
        mode: 'color',
        color: {
          value: 'white'
        }
      }),
      paddingable: new Paddingable({
        settings: {
          paddingTop: 40,
          paddingBottom: 40
        }
      }),
    }
  }

  classStyles() {
    let styles = {}

    switch(this.settings.mode) {
      case 'height-three-quarter':
        styles['min-height'] = '80vh'
        break
      case 'height-full':
        styles['min-height'] = '100vh'
        break
    }

    return styles
  }

  container() {
    return this.children.filter(child => child.scope() == 'container')[0]
  }
}
