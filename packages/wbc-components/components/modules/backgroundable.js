import WbcModule from './wbc_module'
import * as constants from './names'

export default class Backgroundable extends WbcModule {
  constructor(options) {
    super({ name: constants.Backgroundable, ...options })
  }

  defaultSettings() {
    return {
      mode: 'color',
      color: {
        value: 'transparent'
      }
    }
  }

  classes() {
  }

  styles() {
    let styles = {}

    if (this.settings.mode == 'image') {
    } else {
      styles['background'] = this.settings.color.value
    }

    return styles
  }
}