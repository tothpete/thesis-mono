import WbcModule from './wbc_module'
import * as constants from './names'

export default class Paddingable extends WbcModule {
  constructor(options) {
    super({ name: constants.Paddingable, ...options })
  }

  defaultSettings() {
    return {
      paddingTop: 0,
      paddingBottom: 0
    }
  }

  classes() {
    return {}
  }

  styles() {
    let styles = {}
    styles.paddingTop = `${this.settings.paddingTop}px`
    styles.paddingBottom = `${this.settings.paddingBottom}px`
    return styles
  }
}