export default class WbcModule {
  constructor({ name, settings, resources }) {
    this.name = name || 'wbc-module'
    this.settings = Object.assign(this.defaultSettings(), settings)
    this.resources = resources || {}
  }

  defaultSettings() {
    return {}
  }

  moduleResources() {
    return {}
  }
  
  classes() {
    return {}
  }

  styles() {
    return {}
  }
}