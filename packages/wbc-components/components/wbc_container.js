import WbcComponent from './wbc_component'

export default class WbcContainer extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-container'
    super(options)
  }

  scope() {
    return 'container'
  }

  defaultSettings() {
    return {
      direction: 'column'
    }
  }

  classStyles() {
    let styles = {}
    if (this.settings.direction == 'row') {
      styles['display'] = 'flex'
      styles['align-items'] = 'center'
      if (this.settings.justifyContent == 'start') styles['justify-content'] = 'flex-start'
      if (this.settings.justifyContent == 'center') styles['justify-content'] = 'center'
      if (this.settings.justifyContent == 'end') styles['justify-content'] = 'flex-end'
      if (this.settings.justifyContent == 'space-between') styles['justify-content'] = 'space-between'
      if (this.settings.alignItems == 'start') styles['align-items'] = 'flex-start'
      if (this.settings.alignItems == 'center') styles['align-items'] = 'center'
      if (this.settings.alignItems == 'end') styles['align-items'] = 'flex-end'
    }

    return styles
  }

  // Ideas
  // container modes
    // full - stretches to full
    // normal - 1170px has breakpoints made for that
    // thinner - 970px
    // tight - 500/600px
  // side paddings - .75rem, .5rem
    // can adjust that
    // has modes
      // thin, normal, bolder, bold, thicc
    // has specific set by users
    // in rems? 20px 1.25rem -- can have minimal .625 padding
    // 
}
