import WbcComponent from './wbc_component'
import Backgroundable from './modules/backgroundable'
import Paddingable from './modules/paddingable'

export default class WbcHeader extends WbcComponent {
  constructor(options={}) {
    options.name = 'wbc-header'
    super(options)
  }

  scope() {
    return 'header'
  }

  defaultSettings() {
    return {
      align: 'center'
    }
  }

  classStyles() {
    let styles = {}
    
    if (this.settings.align == 'left') styles['text-align'] = 'left'
    if (this.settings.align == 'center') styles['text-align'] = 'center'
    if (this.settings.align == 'right') styles['text-align'] = 'right'

    return styles
  }

  classModules() {
    return {
      backgroundable: new Backgroundable(),
      paddingable: new Paddingable({ settings: { paddingTop: 10, paddingBottom: 10 } })
    }
  }

  container() {
    return this.children.find(child => child.name == 'wbc-container')
  }
}
