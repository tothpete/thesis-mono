import WbcButton from './wbc_button'
import WbcComponent from './wbc_component'
import WbcContainer from './wbc_container'
import WbcFooter from './wbc_footer'
import WbcHeader from './wbc_header'
import WbcHeading from './wbc_heading'
import WbcImage from './wbc_image'
import WbcNav from './wbc_nav'
import WbcNavItem from './wbc_nav_item'
import WbcP from './wbc_p'
import WbcPage from './wbc_page'
import WbcRoot from './wbc_root'
import WbcSection from './wbc_section'
import WbcText from './wbc_text'
import WbcTn from './wbc_tn'

export default {
  'wbc-button': WbcButton,
  'wbc-component': WbcComponent,
  'wbc-container': WbcContainer,
  'wbc-footer': WbcFooter,
  'wbc-header': WbcHeader,
  'wbc-heading': WbcHeading,
  'wbc-image': WbcImage,
  'wbc-nav': WbcNav,
  'wbc-nav-item': WbcNavItem,
  'wbc-page': WbcPage,
  'wbc-p': WbcP,
  'wbc-root': WbcRoot,
  'wbc-section': WbcSection,
  'wbc-text': WbcText,
  'wbc-tn': WbcTn,
}
