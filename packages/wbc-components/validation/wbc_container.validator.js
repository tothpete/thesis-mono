export default {
  children: {
    validateWhitelistedNames: ['wbc-text', 'wbc-image', 'wbc-button']
  }
}