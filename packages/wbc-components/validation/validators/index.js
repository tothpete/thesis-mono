import groupBy from 'lodash/groupBy'
import countBy from 'lodash/countBy'

export const validateUniqueNames = (members=[], names=[]) => {
  const membersByNameCount = countBy(members, 'name')
  const duplicateNames = names
    .filter(name => membersByNameCount[name] && membersByNameCount[name] > 1)

  if (duplicateNames.length) return `Duplicate members ${duplicateNames}`
}

export const validatePresentNames = (members=[], names=[]) => {
  const membersByName = groupBy(members, 'name')
  const missingNames = names
    .filter(name => !membersByName[name])
  
  if (missingNames.length) return `Missing members ${missingNames}`
}

export const validateWhitelistedNames = (members=[], names=[]) => {
  const unwhitelistedMembers = members
    .map(member => member.name)
    .filter(name => !names.includes(name))
  
  if (unwhitelistedMembers.length) return `Unwhitelisted members ${unwhitelistedMembers}`
}