export default {
  'settings.level': {
    inclusion: [1, 2, 3, 4, 5, 6]
  },  
  children: {
    validateWhitelistedNames: ['wbc-tn']
  }
}