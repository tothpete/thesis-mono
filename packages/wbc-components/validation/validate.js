import validate from 'validate.js'
import * as validators from './validators'

validate.validators = Object.assign({}, validate.validators, validators)

export default validate