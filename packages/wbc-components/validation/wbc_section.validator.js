export default {
  children: {
    validateUniqueNames: ['wbc-container'],
    validatePresentNames: ['wbc-container'],
    validateWhitelistedNames: ['wbc-container']
  }
}