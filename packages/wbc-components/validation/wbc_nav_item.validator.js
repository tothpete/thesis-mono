export default {
  'settings.content': {
    presence: {
      allowEmpty: false
    }
  },
  children: {
    validateWhitelistedNames: ['']
  }
}