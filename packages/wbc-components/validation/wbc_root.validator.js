export default {
  children: {
    validateUniqueNames: ['wbc-header', 'wbc-footer'],
    validatePresentNames: ['wbc-header', 'wbc-footer'],
    validateWhitelistedNames: ['wbc-header', 'wbc-footer']
  }
}