export default {
  'wbc-button': 'Button',
  'wbc-footer': 'Footer',
  'wbc-form': 'Form',
  'wbc-header': 'Header',
  'wbc-heading': 'Heading',
  'wbc-image': 'Image',
  'wbc-nav': 'Navigation',
  'wbc-p': 'Paragraph',
  'wbc-section': 'Section',
  'wbc-spacer': 'Spacer',
  'wbc-text': 'Text'
}