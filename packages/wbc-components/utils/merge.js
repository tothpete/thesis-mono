import deepmerge from 'deepmerge'

export default function merge(objects=[]) {
  return deepmerge.all(objects, { arrayMerge: (destinationArray, sourceArray, options) => sourceArray })
}