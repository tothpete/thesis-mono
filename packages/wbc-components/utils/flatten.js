const flattenComponent = (component) => {
  let result = []
  result.push(component)
  
  component.children.forEach(child => {
    result = result.concat(flattenComponent(child))
  })
  return result
}

export default flattenComponent