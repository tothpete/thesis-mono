const state = {
  components: []
}

const getters = {
  components(state, getters, rootState, rootGetters) {
    return state.components.map(component => rootGetters['componentTree/componentsById'][component]).filter(Boolean)
  }
}

const mutations = {
  addMounted(state, payload) {
    state.components.push(payload)
  },
  removeMounted(state, payload) {
    const index = state.components.indexOf(payload)
    state.components.splice(index, index == -1 ? 0 : 1)
  }
}

export default { namespaced: true, state, getters, mutations }