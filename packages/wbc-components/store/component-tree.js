import WbcRoot from 'wbc-components/components/wbc_root'
import WbcPage from 'wbc-components/components/wbc_page'
import flatten from 'wbc-components/utils/flatten'
import { mapTreeToWbc } from 'wbc-components/map-components'
import nav from './component-tree/nav'
import section from './component-tree/section'
import header from './component-tree/header'
import button from './component-tree/button'
import backgroundable from './component-tree/backgroundable'
import * as treeActions from './component-tree/tree-actions'

const state = {
  layout: null,
  page: null,
}

const getters = {
  wbcLayout: (state) => {
    return state.layout ? mapTreeToWbc(state.layout.root) : new WbcRoot()
  },
  wbcPage: (state) => {
    return state.page ? mapTreeToWbc(state.page.root) : new WbcPage()
  },
  components(state, getters) {
    return [...getters.layoutComponents, ...getters.pageComponents]
  },
  componentsById(state, getters) {
    return getters.components.reduce((components, component) => {
      components[component.wbcId] = component
      return components
    }, {})
  },
  layoutComponents(state, getters) {
    return flatten(getters.wbcLayout)
  },
  layoutComponentsById(state, getters) {
    return getters.layoutComponents.reduce((components, component) => {
      components[component.wbcId] = component
      return components
    }, {})
  },
  pageComponents(state, getters) {
    return flatten(getters.wbcPage)
  },
  pageComponentsById(state, getters) {
    return getters.pageComponents.reduce((components, component) => {
      components[component.wbcId] = component
      return components
    }, {})
  }
}

const actions = {
  setCurrentWebsite({ commit }, { layout, page }) {
    commit('setLayout', layout)
    commit('setPage', page)
  },
}

const mutations = {
  setLayout(state, payload) {
    state.layout = payload
  },
  setPage(state, payload) {
    state.page = payload
  },
  insertComponentAfter: (state, { wbcId, newComponent }) => {
    treeActions.insertComponentAfter(state.layout.root, wbcId, newComponent)
    treeActions.insertComponentAfter(state.page.root, wbcId, newComponent)
  },
  insertComponentBefore: (state, { wbcId, newComponent }) => {
    treeActions.insertComponentBefore(state.layout.root, wbcId, newComponent)
    treeActions.insertComponentBefore(state.page.root, wbcId, newComponent)
  },
  appendComponentTo: (state, { wbcId, newComponent }) => {
    treeActions.appendComponentTo(state.layout.root, wbcId, newComponent)
    treeActions.appendComponentTo(state.page.root, wbcId, newComponent)
  },
  updateComponent: (state, { wbcId, newComponent }) => {
    treeActions.updateComponent(state.layout.root, wbcId, newComponent)
    treeActions.updateComponent(state.page.root, wbcId, newComponent)
  },
  removeComponent: (state, { wbcId }) => {
    treeActions.removeComponent(state.layout.root, wbcId)
    treeActions.removeComponent(state.page.root, wbcId)
  }
}

const modules = {
  nav,
  section,
  header,
  button,
  backgroundable
}

export default { namespaced: true, state, getters, actions, mutations, modules }