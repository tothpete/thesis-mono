const actions = {
  changeButtonContent({ commit, getters }, { wbcId, content }) {
    const newComponent = {
      settings: {
        content
      }
    }
    commit('updateComponent', { wbcId, newComponent })
  },
  changeButtonUrl({ commit, getters }, { wbcId, url }) {
    const newComponent = {
      settings: {
        url
      }
    }

    commit('updateComponent', { wbcId, newComponent })
  }  
}

export default { actions }