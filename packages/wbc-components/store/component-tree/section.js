const actions = {
  changeSectionHeightMode({ commit, getters }, { wbcId, mode }) {
    const newComponent = {
      settings: {
        mode
      }
    }

    commit('updateComponent', { wbcId, newComponent })
  }
}

export default { actions }