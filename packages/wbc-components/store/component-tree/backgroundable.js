const actions = {
  changeBackgroundableColor({ commit }, { wbcId, color }) {
    const newComponent = {
      modules: {
        backgroundable: {
          name: 'backgroundable',
          settings: {
            color: {
              value: color
            }
          }
        }
      }
    }
    commit('updateComponent', { wbcId, newComponent })
  }
}

export default { actions }