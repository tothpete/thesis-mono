const actions = {
  changeHeaderAlign({ commit }, { wbcId, align }) {
    const newComponent = {
      settings: {
        align
      }
    }

    commit('updateComponent', { wbcId, newComponent })
  }
}

export default { actions }