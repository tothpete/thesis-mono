const getters = {
  isNavEmpty: () => (navigation) => {
    return !!navigation.children
  }
}

const actions = {
  insertNavItem({ commit, getters }, { newItem, position }) {
  }
}

export default { getters, actions }