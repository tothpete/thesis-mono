import Vue from 'vue'
import merge from 'deepmerge'
import pick from 'lodash/pick'

export const getComponent = (root, wbcId) => {
  if (!root) return
  if (root.wbcId == wbcId) return root

  if (root.children) {
    let result = null
    root.children.forEach(child => { 
      if (!result) result = getComponent(child, wbcId)
    })
    return result
  }

  return null
}

export const getParent = (root={}, wbcId) => {
  if (root.children && root.children.map(child => child.wbcId).includes(wbcId)) return root

  if (root.children) {
    let result = null
    root.children.forEach(child => { 
      if (!result) result = getParent(child, wbcId)
    })
    return result
  }

  return null
}

export const insertComponentAfter = (root, wbcId, newComponent) => {
  const parent = getParent(root, wbcId)
  const component = getComponent(parent, wbcId)
  return parent && parent.children.splice(parent.children.indexOf(component) + 1, 0, newComponent)
}

export const insertComponentBefore = (root, wbcId, newComponent) => {
  const parent = getParent(root, wbcId)
  const component = getComponent(parent, wbcId)
  return parent && parent.children.splice(parent.children.indexOf(component), 0, newComponent)
}

export const appendComponentTo = (root, wbcId, newComponent) => {
  const component = getComponent(root, wbcId)
  if (component) component.children = component.children || []
  return component && component.children.splice(component.children.length, 0, newComponent)
}

export const removeComponent = (root, wbcId) => {
  const parent = getParent(root, wbcId)
  if (parent) {
    const component = getComponent(parent, wbcId)
    return parent.children.splice(parent.children.indexOf(component), 1)
  }
}

export const updateComponent = (root, wbcId, newComponent) => {    
  const component = getComponent(root, wbcId)
  
  if (component) {
    const mergedComponent = merge(component, newComponent, { arrayMerge: (destinationArray, sourceArray, options) => sourceArray})
    const newAttributes = pick(mergedComponent, ['settings', 'modules', 'children', 'resources', 'styles'])

    Object.keys(newAttributes).forEach(attr => {
      Vue.set(component, attr, newAttributes[attr])
    })
  }
}