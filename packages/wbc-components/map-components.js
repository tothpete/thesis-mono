import wbcComponents from './components'
import wbcModules from './components/modules'

export const mapTreeToWbc = (currentComponent) => {
  let wbcComponent = mapComponentToWbc(currentComponent)
  wbcComponent.children = wbcComponent.children.map(child => mapTreeToWbc(child))
  return wbcComponent
}

const mapComponentToWbc = (component={}) => {
  const Component = wbcComponents[component.name]

  if (!Component) throw Error(`Unknown component for ${JSON.stringify(component)}`)
  
  const modules = Object
    .keys(component.modules || {})
    .reduce((modules, currentModule) => {
      modules[currentModule] = mapComponentModuleToWbc(component.modules[currentModule])
      return modules
    }, {})

  let wbcComponent = new Component({ ...component, modules })

  return wbcComponent
}

const mapComponentModuleToWbc = (componentModule={}) => {
  const ComponentModule = wbcModules[componentModule.name]

  if (!ComponentModule) throw Error(`Unknown module for ${JSON.stringify(componentModule)}`)

  return new ComponentModule(componentModule)
}