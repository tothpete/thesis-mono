import validate from '../../validation/validate'
import wbcNavValidator from '../../validation/wbc_nav.validator'

import WbcNav from '../../components/wbc_nav'
import WbcNavItem from '../../components/wbc_nav_item'
import WbcText from '../../components/wbc_text'

test('Navigation does whitelist only navigation item', () => {
  expect(validate(new WbcNav({ children: [new WbcNavItem()] }), wbcNavValidator)).toBeFalsy()
  expect(validate(new WbcNav({ children: [new WbcNavItem(), new WbcText()] }), wbcNavValidator)).toBeTruthy()
})