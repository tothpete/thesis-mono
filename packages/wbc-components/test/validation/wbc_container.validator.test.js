import validate from '../../validation/validate'
import wbcContainerValidator from '../../validation/wbc_container.validator'

import WbcContainer from '../../components/wbc_button'
import WbcText from '../../components/wbc_text'
import WbcImage from '../../components/wbc_image'
import WbcButton from '../../components/wbc_button'
import WbcSection from '../../components/wbc_section'

test('Container does whitelist only text, image and button', () => {
  expect(validate(new WbcContainer({ children: [new WbcText(), new WbcImage(), new WbcButton()] }), wbcContainerValidator)).toBeFalsy()
  expect(validate(new WbcContainer({ children: [new WbcText(), new WbcImage(), new WbcButton(), new WbcSection()] }), wbcContainerValidator)).toBeTruthy()
})