import validate from '../../validation/validate'
import wbcRootValidator from '../../validation/wbc_root.validator'

import WbcRoot from '../../components/wbc_root'
import WbcHeader from '../../components/wbc_header'
import WbcFooter from '../../components/wbc_footer'
import WbcSection from '../../components/wbc_section'

test('Root does whitelist only header and footer', () => {
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcFooter()] }), wbcRootValidator)).toBeFalsy()
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcFooter(), new WbcSection()] }), wbcRootValidator)).toBeTruthy()
})

test('Root must contain header and footer', () => {
  expect(validate(new WbcRoot({ children: [] }), wbcRootValidator)).toBeTruthy()
  expect(validate(new WbcRoot({ children: [new WbcHeader()] }), wbcRootValidator)).toBeTruthy()
  expect(validate(new WbcRoot({ children: [new WbcFooter()] }), wbcRootValidator)).toBeTruthy()
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcFooter()] }), wbcRootValidator)).toBeFalsy()
})

test('Root has unique header', () => {
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcHeader(), new WbcFooter()] }), wbcRootValidator)).toBeTruthy()
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcFooter()] }), wbcRootValidator)).toBeFalsy()
})

test('Root has unique footer', () => {
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcFooter(), new WbcFooter()] }), wbcRootValidator)).toBeTruthy()
  expect(validate(new WbcRoot({ children: [new WbcHeader(), new WbcFooter()] }), wbcRootValidator)).toBeFalsy()
})