import validate from '../../validation/validate'
import wbcTnValidator from '../../validation/wbc_tn.validator'

import WbcTn from '../../components/wbc_tn'
import WbcContainer from '../../components/wbc_container'

test('Text node does not whitelist any children', () => {
  expect(validate(new WbcTn({ settings: { content: 'sample' }, children: [] }), wbcTnValidator)).toBeFalsy()
  expect(validate(new WbcTn({ children: [new WbcContainer] }), wbcTnValidator)).toBeTruthy()
})


test('Text node does not have empty content', () => {
  expect(validate(new WbcTn({ settings: { content: 'sample' }, children: [] }), wbcTnValidator)).toBeFalsy()
  expect(validate(new WbcTn({ settings: { content: '' } }), wbcTnValidator)).toBeTruthy()
})