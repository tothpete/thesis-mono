import validate from '../../validation/validate'
import wbcHeadingValidator from '../../validation/wbc_heading.validator'

import WbcHeading from '../../components/wbc_header'
import WbcTn from '../../components/wbc_tn'
import WbcP from '../../components/wbc_p'

test('Heading does whitelist only text node', () => {
  expect(validate(new WbcHeading({ children: [new WbcTn()] }), wbcHeadingValidator)).toBeFalsy()
  expect(validate(new WbcHeading({ children: [new WbcTn(), new WbcP()] }), wbcHeadingValidator)).toBeTruthy()
})