import validate from '../../validation/validate'
import wbcSectionValidator from '../../validation/wbc_section.validator'

import WbcSection from '../../components/wbc_section'
import WbcContainer from '../../components/wbc_container'
import WbcText from '../../components/wbc_text'

test('Section does whitelist only container', () => {
  expect(validate(new WbcSection({ children: [new WbcContainer()] }), wbcSectionValidator)).toBeFalsy()
  expect(validate(new WbcSection({ children: [new WbcContainer(), new WbcText()] }), wbcSectionValidator)).toBeTruthy()
})

test('Section must contain container', () => {
  expect(validate(new WbcSection({ children: [new WbcContainer()] }), wbcSectionValidator)).toBeFalsy()
  expect(validate(new WbcSection({ children: [] }), wbcSectionValidator)).toBeTruthy()
})

test('Section has unique container', () => {
  expect(validate(new WbcSection({ children: [new WbcContainer()] }), wbcSectionValidator)).toBeFalsy()
  expect(validate(new WbcSection({ children: [new WbcContainer(), new WbcContainer()] }), wbcSectionValidator)).toBeTruthy()
})