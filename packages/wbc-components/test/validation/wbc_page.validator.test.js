import validate from '../../validation/validate'
import wbcPageValidator from '../../validation/wbc_page.validator'

import WbcPage from '../../components/wbc_page'
import WbcSection from '../../components/wbc_section'
import WbcText from '../../components/wbc_text'

test('Page does whitelist only section children', () => {
  expect(validate(new WbcPage({ children: [new WbcSection()] }), wbcPageValidator)).toBeFalsy()
  expect(validate(new WbcPage({ children: [new WbcSection(), new WbcText()] }), wbcPageValidator)).toBeTruthy()
})