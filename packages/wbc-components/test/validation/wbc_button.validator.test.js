import validate from '../../validation/validate'
import wbcButtonValidator from '../../validation/wbc_button.validator'

import WbcButton from '../../components/wbc_button'
import WbcText from '../../components/wbc_text'

test('Button does not whitelist any children', () => {
  expect(validate(new WbcButton({ children: [] }), wbcButtonValidator)).toBeFalsy()
  expect(validate(new WbcButton({ children: [new WbcText()] }), wbcButtonValidator)).toBeTruthy()
})

test('Button does not allow empty content', () => {
  expect(validate(new WbcButton({ settings: { content: '' } }), wbcButtonValidator)).toBeTruthy()
})