import validate from '../../validation/validate'
import wbcPValidator from '../../validation/wbc_p.validator'

import WbcP from '../../components/wbc_p'
import WbcTn from '../../components/wbc_tn'
import WbcText from '../../components/wbc_text'

test('Navigation does whitelist only text node', () => {
  expect(validate(new WbcP({ children: [new WbcTn()] }), wbcPValidator)).toBeFalsy()
  expect(validate(new WbcP({ children: [new WbcTn(), new WbcText()] }), wbcPValidator)).toBeTruthy()
})