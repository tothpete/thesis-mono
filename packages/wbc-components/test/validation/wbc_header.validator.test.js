import validate from '../../validation/validate'
import wbcHeaderValidator from '../../validation/wbc_header.validator'

import WbcHeader from '../../components/wbc_header'
import WbcNav from '../../components/wbc_nav'
import WbcText from '../../components/wbc_text'

test('Header does whitelist only navigation', () => {
  expect(validate(new WbcHeader({ children: [new WbcNav()] }), wbcHeaderValidator)).toBeFalsy()
  expect(validate(new WbcHeader({ children: [new WbcNav(), new WbcText()] }), wbcHeaderValidator)).toBeTruthy()
})