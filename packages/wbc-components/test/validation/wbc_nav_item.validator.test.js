import validate from '../../validation/validate'
import wbcNavItemValidator from '../../validation/wbc_nav_item.validator'

import WbcNavItem from '../../components/wbc_nav_item'
import WbcText from '../../components/wbc_text'

test('Navigation item does not whitelist any children', () => {
  expect(validate(new WbcNavItem({ children: [] }), wbcNavItemValidator)).toBeFalsy()
  expect(validate(new WbcNavItem({ children: [new WbcText()] }), wbcNavItemValidator)).toBeTruthy()
})

test('Navigation item does not have empty content', () => {
  expect(validate(new WbcNavItem({ settings: { content: '' } }), wbcNavItemValidator)).toBeTruthy()
  expect(validate(new WbcNavItem({ settings: { content: 'Item content' } }), wbcNavItemValidator)).toBeFalsy()
})