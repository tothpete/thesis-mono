import validate from '../../validation/validate'
import wbcTextValidator from '../../validation/wbc_text.validator'

import WbcText from '../../components/wbc_text'
import WbcHeading from '../../components/wbc_heading'
import WbcP from '../../components/wbc_p'
import WbcContainer from '../../components/wbc_container'

test('Text does whitelist heading and paragraph', () => {
  expect(validate(new WbcText({ children: [] }), wbcTextValidator)).toBeFalsy()
  expect(validate(new WbcText({ children: [new WbcHeading(), new WbcP()] }), wbcTextValidator)).toBeFalsy()
  expect(validate(new WbcText({ children: [new WbcHeading(), new WbcP(), new WbcContainer] }), wbcTextValidator)).toBeTruthy()
})