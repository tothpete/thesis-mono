import backgroundable from './backgroundable'
import paddingable from './paddingable'

export default {
  backgroundable,
  paddingable,
}