import modules from './modules'
import wbcButton from './wbc-button'
import wbcContainer from './wbc-container'
import wbcFooter from './wbc-footer'
import wbcHeader from './wbc-header'
import wbcHeading from './wbc-heading'
import wbcImage from './wbc-image'
import wbcNav from './wbc-nav'
import wbcNavItem from './wbc-nav-item'
import wbcP from './wbc-p'
import wbcPage from './wbc-page'
import wbcRoot from './wbc-root'
import wbcSection from './wbc-section'
import wbcText from './wbc-text'
import wbcTn from './wbc-tn'

export default {
  'wbc-button': wbcButton,
  'wbc-container': wbcContainer,
  'wbc-footer': wbcFooter,
  'wbc-header': wbcHeader,
  'wbc-heading': wbcHeading,
  'wbc-image': wbcImage,
  'wbc-nav': wbcNav,
  'wbc-nav-item': wbcNavItem,
  'wbc-page': wbcPage,
  'wbc-p': wbcP,
  'wbc-root': wbcRoot,
  'wbc-section': wbcSection,
  'wbc-text': wbcText,
  'wbc-tn': wbcTn,
  modules
}
