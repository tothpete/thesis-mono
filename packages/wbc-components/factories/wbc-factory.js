import WbcSection from '../components/wbc_section'
import WbcText from '../components/wbc_text'
import WbcNav from '../components/wbc_nav'
import WbcImage from '../components/wbc_image'
import WbcContainer from '../components/wbc_container'
import WbcRoot from '../components/wbc_root'
import WbcHeader from '../components/wbc_header'
import WbcFooter from '../components/wbc_footer'
import WbcPage from '../components/wbc_page'
import WbcButton from '../components/wbc_button'

const serialize = component => JSON.parse(JSON.stringify(component))

export const layout = () => serialize(new WbcRoot({
  children: [
    new WbcHeader(),
    new WbcFooter({
      children: [
        new WbcContainer()
      ]
    })
  ]
}))

export const page = () => serialize(new WbcPage())

export const section = () => serialize(new WbcSection({
  children: [
    new WbcContainer()
  ]
}))

export const button = () => serialize(new WbcButton())
export const text = () => serialize(new WbcText())
export const image = () => serialize(new WbcImage())
export const nav = () => serialize(new WbcNav())