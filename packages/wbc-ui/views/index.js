import uiButton from './ui-button'
import uiComponent from './ui-component'
import uiContainer from './ui-container'
import uiFlex from './ui-flex'
import uiH from './ui-h'
import uiP from './ui-p'
import uiPopover from './ui-popover'
import uiSelect from './ui-select'
import uiTabs from './ui-tabs'
import uiTab from './ui-tab'
import uiTabcontent from './ui-tabcontent'
import uiTextField from './ui-text-field'
import uiWindow from './ui-window'

export default {
  uiButton,
  uiComponent,
  uiContainer,
  uiFlex,
  uiH,
  uiP,
  uiPopover,
  uiSelect,
  uiTabs,
  uiTab,
  uiTabcontent,
  uiTextField,
  uiWindow
}