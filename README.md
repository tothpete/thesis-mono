# Installation guide

## Prerequisites

* Node.js installed - version >= v10.15
* Mongodb installed

## Setup

* $ npm install -g lerna
* $ lerna bootstrap
* $ cd packages/wbc-admin
* $ npm install
* $ npm run dev